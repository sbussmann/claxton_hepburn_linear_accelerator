##############################################################################
# Project: Kish - Major Joint Replacement 
# Date: 5/19/14
# Author: Sam Bussmann
# Description: Demographic Output
# Notes: 
##############################################################################

coeff<-as.data.frame(as.matrix(coef(cvnet1,s = "lambda.min")))
coeff<-data.frame(Varname=row.names(coeff),Value=coeff[,"1"])
write.csv(coeff,file="coeff.csv")


### Income plot of top deciles

intop_drg <- (pm$CommunityPersonID %in% tog_5$CommunityPersonID )

par(mfrow=c(2,1))
hist(pm$FindIncome[intop_drg],xlim=c(0,250000),breaks=25,xlab="Income",main="Income of Top 20k Scored Records",
     col="lightblue")

hist(as.numeric(pm$ExactAge[intop_drg & !is.na(pm$ExactAge)]),xlim=c(20,90),breaks=20,
     xlab="Age",main="Age of Top 20k Scored Records",col="lightgreen")
